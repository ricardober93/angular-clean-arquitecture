
import { Observable } from 'rxjs';
import { TodoModel } from './todo-list';

export abstract class TodoListRepository {
  abstract getTodoById(id: number): Observable<TodoModel>;
  abstract getAllTodos(): Observable<TodoModel>;
}
