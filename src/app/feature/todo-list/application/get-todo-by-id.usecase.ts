import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TodoModel } from '../domain/todo-list';
import { TodoListRepository } from '../domain/todo-list.repository';

@Injectable({
  providedIn: 'root'
})
export class GetTodoListByIdUseCase  {

  constructor(private todoListRepository: TodoListRepository) { }

  execute(params: number): Observable<TodoModel> {
    return this.todoListRepository.getTodoById(params);
  }
}
