import { TodoModel } from "../domain/todo-list";
import { TodoListDTO } from "../infraestructure/todo-list.dto";


function createRandomTodoList() {
  return Math.random() * 100
}
export class TodoWebRepositoryMapper {


  mapFrom(param: TodoListDTO): TodoModel {
    return {
      id: param.id,
      description: param.description,
      completed: param.completed,
      meta: param.meta
    };
  }


  mapTo(param: TodoModel): TodoListDTO {
    return {
      id: param.id,
      description: param.description,
      completed: param.completed,
      meta: param.meta ?? null
    };
  }
}
