export interface TodoListDTO {
  id: number;
  description: string;
  completed:   boolean;
  meta:        Meta | null;
}

export interface Meta {
  assigned_to: string;
  priority:    string;
  notes:       string;
}
