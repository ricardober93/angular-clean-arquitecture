import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, flatMap, map } from "rxjs";
import { TodoWebRepositoryMapper } from "../application/Todo-list-RepositoryMapper";
import { TodoModel } from "../domain/todo-list";
import { TodoListRepository } from "../domain/todo-list.repository";
import { TodoListDTO } from "./todo-list.dto";

@Injectable({
  providedIn: 'root'
})
export class TodoListWebRepository extends TodoListRepository {



  mapper = new TodoWebRepositoryMapper();

  constructor(
    private http: HttpClient
  ) {
    super();
  }

  override getTodoById(id: number): Observable<TodoModel> {
    return this.http
      .get<TodoListDTO>(`https://todos.simpleapi.dev/api/todos/${id}?apikey=ae83d358-9cf3-44f1-82ab-138625779017`)
      .pipe(map(this.mapper.mapFrom));
  }
  override getAllTodos(): Observable<TodoModel> {
    return this.http
    .get<TodoListDTO[]>('https://todos.simpleapi.dev/api/todos?apikey=ae83d358-9cf3-44f1-82ab-138625779017')
    .pipe(flatMap((item) => item))
    .pipe(map(this.mapper.mapFrom));
  }


}
