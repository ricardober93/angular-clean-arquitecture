import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TodoListRepository } from './domain/todo-list.repository';
import { TodoListWebRepository } from './infraestructure/todo-list.repository.impl';
import { TodoListComponent } from './presetacion/components/todo-list/todo-list.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    TodoListComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    TodoListComponent
  ],
  providers: [
    {provide: TodoListRepository, useClass: TodoListWebRepository}
  ],
})
export class TodoListModule { }
