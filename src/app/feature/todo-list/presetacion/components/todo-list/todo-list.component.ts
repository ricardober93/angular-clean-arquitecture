import { Component, OnInit } from "@angular/core";
import { GetAllTodoListUsecase } from "../../../application/create-todo-list.usecase";
import { TodoModel } from "../../../domain/todo-list";
import { GetTodoListByIdUseCase } from "../../../application/get-todo-by-id.usecase";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  idx: string = '';
  todoList: TodoModel[] = [];
  todo: TodoModel = {} as TodoModel;

	constructor(
    private getAllTodoListUsecase: GetAllTodoListUsecase,
    private getTodoListByIdUseCase: GetTodoListByIdUseCase
    ) {}

  ngOnInit(): void {
   this.getAllTodoListUsecase.execute().subscribe(data => this.todoList.push(data));
  }

  search(): void {
    this.getById(this.idx)
  }

  getById(id: string): void {
    const payload = Number(id);
    this.getTodoListByIdUseCase.execute(payload).subscribe(data => this.todo = data);
  }
}
